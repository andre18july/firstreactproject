import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';


const app = props => {

    const [personsState, setPersonsState] = useState({
      persons: [
        { name: 'Max', age: 29 },
        { name: 'Manu', age: 30 },
        { name: 'John', age: 51 }
      ], 
    });

    const [otherState, setOtherState] = useState({
      otherState: 'Some other value'
    })

    console.log(personsState, otherState);

    const switchNameHandler = () => {
      personsState.persons[0].name = 'Andre';
      setPersonsState({
        persons: [
          { name: 'Andre', age: 29 },
          { name: 'Manu', age: 30 },
          { name: 'John', age: 51 }
        ]
      })
    };

    return (
      <div className="App">
        <h1>Hello World - React APP</h1>
        <button onClick={switchNameHandler}>Switch Name</button>
        <Person 
          name={personsState.persons[0].name} 
          age={personsState.persons[0].age}/>
        <Person 
          name={personsState.persons[1].name} 
          age={personsState.persons[1].age}>Hobbies: Soccer</Person>
        <Person 
          name={personsState.persons[2].name} 
          age={personsState.persons[2].age}/>
      </div>
    );
    
   //return React.createElement('div', {className: 'App'}, React.createElement('h1', '' , 'Hello, this is React!!!'));

}






export default app;
