import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';


class App extends Component{

    state = {
      persons: [
        { id: 'p1', name: 'Max', age: 29 },
        { id: 'p2', name: 'Manu', age: 30 },
        { id: 'p3', name: 'John', age: 51 }
      ],
      showPersons: false  
    };

    switchNameHandler = (newName) => {
      this.setState({
        persons: [
          { id: 'p1', name: newName, age: 29 },
          { id: 'p2', name: 'Manu', age: 30 },
          { id: 'p3', name: 'John', age: 51 }
        ]
      })
    };

    nameChangedHandler = (event, id) => {

      const personIndex = this.state.persons.findIndex(p => p.id === id);

     
      //const person = this.state.persons[personIndex];
      const person = {
        ...this.state.persons[personIndex]
      };

      person.name = event.target.value;

      const persons = [...this.state.persons];
      persons[personIndex] = person;

      this.setState({
        persons: persons
      })
    };



    togglePersonsHandler = () => {
      const doesShow = this.state.showPersons;
      this.setState({
        showPersons: !doesShow
      });
    }


    deletePersonHandler = (personIndex) => {
      //const persons = this.state.persons.slice();
      const persons = [...this.state.persons];
      persons.splice(personIndex, 1);
      this.setState({persons: persons});
    }

    render(){
      const style = {
        backgroundColor: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '9px',
        cursor: 'pointer'
      };

      let persons = null;

      if(this.state.showPersons){
        persons = (
          <div>
            {this.state.persons.map((person, index) => {
              return <Person 
                click={() => this.deletePersonHandler(index)}
                name={person.name} 
                age={person.age}
                key={person.id}
                changed={(event) => this.nameChangedHandler(event, person.id)}
                />
            })}
          </div>
        );
      }

      return (
        <div className="App">
          <h1>Hello World - React APP</h1>
          <button 
            style={style}
            onClick={() => this.switchNameHandler('Andre')}>Switch Name</button>

          <button 
            style={style}
            onClick={this.togglePersonsHandler}>Toggle Persons</button>

          {persons}
          
        </div>
      );
    }
    
   //return React.createElement('div', {className: 'App'}, React.createElement('h1', '' , 'Hello, this is React!!!'));


   render2(){
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '9px',
      cursor: 'pointer'
    };

    return (
      <div className="App">
        <h1>Hello World - React APP</h1>
        <button 
          style={style}
          onClick={() => this.switchNameHandler('Andre')}>Switch Name</button>

        <button 
          style={style}
          onClick={this.togglePersonsHandler}>Toggle Persons</button>



        
        {
        this.state.showPersons ? 
        <div>
          <Person 
            name={this.state.persons[0].name} 
            age={this.state.persons[0].age}/>
          <Person 
            name={this.state.persons[1].name} 
            age={this.state.persons[1].age}
            click={this.switchNameHandler.bind(this, 'Andre II')} 
            changed={this.nameChangedHandler} >Hobbies: Soccer</Person>
          <Person 
            name={this.state.persons[2].name} 
            age={this.state.persons[2].age}/>
         </div> : null
        }
        
        
      </div>
    );
  }

}


export default App;
